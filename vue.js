var app = new Vue({
    el: '#app',
    data: {
      api: 'https://jsonplaceholder.typicode.com/users',
      errored: false,
      name: null,
      email: null,
      users:[
        {id: 11, name: 'test', email:'user@ukr.net'},
        {id: 12, name: 'name', email:'user@ukr.net'},
        {id: 13, name: 'name', email:'user@ukr.net'},
        {id: 14, name: 'test', email:'user@ukr.net'}
      ],
      filterUser: null,
      searchText: null,
      selectedId: '',
    },

    computed: {
      countUser: function() {
        return this.filterUser == null ? this.users.length : this.filterUser.length;
      },

      getUsers: function() {
        return this.filterUser == null ? this.users : this.filterUser;
      },

    },

    methods: {
      addUser: function () {
        if (this.name != null && this.email != null) {
          this.users.push({
            id: this.getId(),
            name: this.name,
            email: this.email
          })
        } else {
          alert('name or email empty');
        }
      },

      getId: function() {
        id = 0;
        for (key in this.users) {
          if (id < this.users[key].id) {
            id = this.users[key].id;
          }
        }
        return ++id;
      },

      updateUser: function() {
        for (key in this.users) {
          if (this.selectedId == this.users[key].id) {
            this.users[key].name = this.name;
            this.users[key].email = this.email;
          }
        }
      },

      removeUser(index) {
        this.users.splice(index, 1);
      },

      search: function () {
        if (this.searchText != null) {
          this.filterUser = [];
          for (key in this.users) {
            if (this.users[key].name.toLowerCase().indexOf(this.searchText.toLowerCase()) != -1) {
              this.filterUser.push(
                this.users[key]
              );
            }
          }
        }
      },

      clearSearch: function() {
        this.filterUser = null;
      },

    },

    mounted() {
      axios
        .get(this.api)
        .then(response => (this.users = [...this.users, ...response.data]))
        .catch(error => {
          console.log(error);
          this.errored = true;
        });
    }
  })
